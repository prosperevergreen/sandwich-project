import axios from 'axios';




export const getAllOrders = () => {
    setUrl();
    return axios.get('/order');       
}


export const getOrderbyId = (id) => {
    setUrl();
    return axios.get(`/order/${id}`);       
}


const setUrl = () => {    
    axios.defaults.baseURL = `http://localhost:5001`;

}