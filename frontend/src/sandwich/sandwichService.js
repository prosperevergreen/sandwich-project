import axios from 'axios';




export const getAllSandwiches = () => {
    setUrl();
    return axios.get('/sandwich');       
}



export const addNewSandwich = (sandwich) => {
    setUrl();
    return axios.post('/sandwich',sandwich);       
}


export const getSandwichById = (id) => {
    setUrl();
    return axios.get(`/sandwich/${id}`);       
}


export const deleteSandwich = (id) => {
    setUrl();
    return axios.delete(`/sandwich/${id}`);       
}




export const updateSandwich = (sandwich) => {
    setUrl();
    return axios.put(`/sandwich/${sandwich._id}`,sandwich);       
}



export const getAllToppings = () => {
    setUrl();
    return axios.get('/toppings');       
}

const setUrl = () => {    
    axios.defaults.baseURL = `http://localhost:5001`;

}