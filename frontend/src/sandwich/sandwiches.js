import React, { useEffect } from 'react';
import { connect } from "react-redux";

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button';
// import Modal from 'react-bootstrap/Modal'
// import Form from 'react-bootstrap/Form'
import { PageHeader } from './../components/pageHeader'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'

import ButtonGroup from 'react-bootstrap/ButtonGroup'


import { fetchSandwichList, fetchToppingList, setSelectedToppingList, addToCart, setShowToppingSelection, setSelectedSandwichId, setRedirectToList, deleteSandwich } from './sandwichAction';



const Sandwiches = ({ fetchSandwichList, addToCart, sandwichList, setRedirectToList, deleteSandwich }) => {



    useEffect(() => {

        setRedirectToList(false);

    }, [setRedirectToList])


    useEffect(() => {

        fetchSandwichList();

    }, [fetchSandwichList])


    const chunk = (arr, chunkSize = 1, cache = []) => {
        const tmp = [...arr]
        if (chunkSize <= 0) return cache
        while (tmp.length) cache.push(tmp.splice(0, chunkSize))
        return cache
    }

    // const showToppingModal = (event) => {
    //     console.log(event.target.value)
    //     setSelectedSandwichId(event.target.value)
    //     setShowToppingSelection(true)

    // }


    // const hideToppingModal = () => {
    //     setShowToppingSelection(false)
    //     setSelectedSandwichId(0)
    //     setSelectedToppingList([])

    // }


    const handleAddToppingsAndAddToCart = (event) => {

        console.log(event.target.value)
        const sandwichToCart = sandwichList.find(sandwich => sandwich._id === event.target.value)
        console.log(sandwichToCart)
        // sandwichToCart.toppings = [...selectedToppingList];
        addToCart(sandwichToCart)
        // setShowToppingSelection(false)
        // setSelectedToppingList([])


    }



    const handleDelete = (id) => {
        deleteSandwich(id)
    }


    // const handleToppingToggle = (event) => {
    //     if (event.target.checked) {
    //         setSelectedToppingList([...selectedToppingList, event.target.value])
    //     } else {
    //         const currentTopptings = [...selectedToppingList]
    //         const findTopping = currentTopptings.find(topping => topping === event.target.value);
    //         if (findTopping) {
    //             const newList = currentTopptings.filter(topping => topping !== findTopping);
    //             setSelectedToppingList([...newList]);
    //         }
    //     }
    // }



    const sandwichesChunks = chunk(sandwichList, 4);


    return (
        <Container >

            <PageHeader headerName='Sandwich Menu' />



            {
                sandwichesChunks.map((sandwichChunk, index) => {
                    const sandwichCols = sandwichChunk.map((sandwich, index) => {
                        return (
                            <Col xs="3" key={index}>

                                <Card
                                    bg='light'
                                    key={index}
                                    text='dark'
                                    style={{ boxShadow: '0 10px 16px 0', height: '40rem' }}
                                    className="mb-2">

                                    <Card.Img variant="top" height={250} src={sandwich.sandwhichImage} />

                                    <Card.Body>
                                        <Card.Title>{sandwich.name} </Card.Title>
                                        <Card.Text>
                                            {sandwich.description}

                                            <hr />
                                            Toppings
                                            <hr />
                                            <ul>
                                                {
                                                    sandwich.toppings.map((topping, index) => <li key={index}>{topping.name}</li>)

                                                }

                                            </ul>
                                        </Card.Text>
                                    </Card.Body>
                                    {/* <Card.Footer><Button variant='info' value={sandwich._id} onClick={handleAddToppingsAndAddToCart}>  Add to Cart</Button></Card.Footer> */}
                                    <Card.Footer>
                                        <Row>
                                            <Col><Button variant='info' value={sandwich._id} onClick={handleAddToppingsAndAddToCart}>Add to Cart</Button></Col>
                                            <Col>   <Dropdown>
                                                <Dropdown.Toggle variant="info" id="dropdown-basic">
                                                    Actions
                                            </Dropdown.Toggle>

                                                <Dropdown.Menu>
                                                    {/* <Dropdown.Item >Update</Dropdown.Item> */}
                                                    <Dropdown.Item onClick={() => handleDelete(sandwich._id)}>Delete</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown></Col>
                                        </Row>


                                    </Card.Footer>

                                </Card>

                            </Col>
                        );
                    });
                    return <Row key={index}>{sandwichCols}</Row>
                })



            }


            {/* {
                <Form>

                    <Modal
                        size="lg"
                        show={showToppingSelection}
                        aria-labelledby="contained-modal-title-vcenter"
                        centered>
                        <Modal.Header>
                            <Modal.Title id="contained-modal-title-vcenter">
                                Add Toppings
                        </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>

                            {
                                toppingList.map((topping, index) => {
                                    return (

                                        <Form.Check
                                            key={index}
                                            type="switch"
                                            id={`custom-switch-${index}`}
                                            label={topping.name}
                                            value={topping.name}
                                            onChange={handleToppingToggle}
                                        />
                                    )
                                })
                            }
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={handleAddToppingsAndAddToCart} type="submit">Add to Cart</Button>
                            <Button onClick={hideToppingModal}>Cancel</Button>
                        </Modal.Footer>
                    </Modal>
                </Form>


            } */}



        </Container>

    );



}


const mapStateToProps = ({ SandwichReducer }) => ({
    sandwichList: SandwichReducer.sandwichList,
    toppingList: SandwichReducer.toppingList,
    showToppingSelection: SandwichReducer.showToppingSelection,
    selectedSandwichId: SandwichReducer.selectedSandwichId,
    selectedToppingList: SandwichReducer.selectedToppingList,
    orderItems: SandwichReducer.orderItems,

})

const mapDispatchToProps = (dispatch) => ({
    fetchSandwichList: () => dispatch(fetchSandwichList()),
    fetchToppingList: () => dispatch(fetchToppingList()),
    addToCart: (sandwich) => dispatch(addToCart(sandwich)),
    setSelectedToppingList: (selectedToppings) => dispatch(setSelectedToppingList(selectedToppings)),
    setShowToppingSelection: (showToppingSelection) => dispatch(setShowToppingSelection(showToppingSelection)),
    setSelectedSandwichId: (selectedSandwichId) => dispatch(setSelectedSandwichId(selectedSandwichId)),
    setRedirectToList: (redirectToList) => dispatch(setRedirectToList(redirectToList)),
    deleteSandwich: (id) => dispatch(deleteSandwich(id)),




})




export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Sandwiches);



