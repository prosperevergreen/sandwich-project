import React, { useEffect, useState } from 'react';
import { connect } from "react-redux";

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button';
import { PageHeader } from './../components/pageHeader'
import Form from 'react-bootstrap/Form';

import { setCreateSandwichFormValues, setCreateSandwich } from './sandwichAction';
import { Redirect } from 'react-router-dom'

const SandwichFrom = ({ createdSandwich, setCreateSandwichFormValues, setCreateSandwich, redirectToList }) => {




    const handleChange = (event) => {
        setCreateSandwichFormValues({ ...createdSandwich, [event.target.name]: event.target.value })
    }


    const handleSubmit = (event) => {
        event.preventDefault()
        setCreateSandwich({ ...createdSandwich, imageUrl: `sandwich${Math.floor(Math.random() * 6) + 1}`, toppings: [{ name: 'Cheese' }, { name: 'Salt' }] })
    }

    if (redirectToList) {
        return <Redirect to={`/`} />;

    } else {
        return (

            <Container >

                <PageHeader headerName='Sandwich Form' />


                <Form onSubmit={handleSubmit} >

                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridfName">
                            <Form.Label>Sandwich Name</Form.Label>
                            <Form.Control required name="name" placeholder="Sandwich Name" value={createdSandwich.name} onChange={handleChange} />
                        </Form.Group>


                    </Form.Row>


                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridlDesc">
                            <Form.Label>Description</Form.Label>
                            <Form.Control required name="description" placeholder="Description" value={createdSandwich.description} onChange={handleChange} />
                        </Form.Group>
                    </Form.Row>


                    <Form.Row>
                        <Form.Group controlId="formGridBreadType">
                            <Form.Label>Bread Type</Form.Label>
                            <Form.Control as="select" name="breadType" value={createdSandwich.breadType} onChange={handleChange}>
                                <option value='Oat'>Oat</option>
                                <option value='Rye'>Rye</option>
                                <option value='Wheat'>Wheat</option>
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>


                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridlPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type='number' required name="price" placeholder="Price in Euros" value={parseInt(createdSandwich.price)} onChange={handleChange} />
                        </Form.Group>
                    </Form.Row>

                    <Button variant="info" type="submit">Submit</Button>


                </Form>




            </Container>


        )

    }





}


const mapStateToProps = ({ SandwichReducer }) => ({
    sandwichList: SandwichReducer.sandwichList,
    toppingList: SandwichReducer.toppingList,
    showToppingSelection: SandwichReducer.showToppingSelection,
    selectedSandwichId: SandwichReducer.selectedSandwichId,
    selectedToppingList: SandwichReducer.selectedToppingList,
    orderItems: SandwichReducer.orderItems,
    createdSandwich: SandwichReducer.createdSandwich,
    redirectToList: SandwichReducer.redirectToList


})

const mapDispatchToProps = (dispatch) => ({
    setCreateSandwichFormValues: (sandwich) => dispatch(setCreateSandwichFormValues(sandwich)),
    setCreateSandwich: (sandwich) => dispatch(setCreateSandwich(sandwich)),


})




export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SandwichFrom);
