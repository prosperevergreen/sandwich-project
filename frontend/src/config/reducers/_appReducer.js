import SandwichReducer from '../../sandwich/sandwichReducer';
import CartReducer from '../../cart/cartReducer';
import OrderReducer from '../../order/orderReducer';





let userReducers = Object.assign({}, 
  SandwichReducer,
  CartReducer,
  OrderReducer

);

export default userReducers;