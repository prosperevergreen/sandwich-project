import SandwichSagas from './../../sandwich/sandwichSaga'
import OrderSagas from './../../order/orderSaga'
import CartSagas from './../../cart/cartSaga'



let appSagas = [];

export default appSagas;

appSagas.push(SandwichSagas);
appSagas.push(OrderSagas);
appSagas.push(CartSagas);