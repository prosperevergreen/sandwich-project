import React from 'react';
import { connect } from "react-redux";
import { Navbar, Nav, Container } from 'react-bootstrap';

import { LinkContainer } from 'react-router-bootstrap';
import { FaGlassCheers,FaShoppingCart } from 'react-icons/fa';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';






function Navigation({ orderItems }) {
    return (
        <Container>
            <Row>
                <Col>

                    <Navbar fixed='top' expand="xl" bg="light" variant="light" className="shadow-lg  bg-white rounded">
                        <LinkContainer to="/">

                            <Navbar.Brand>Sandwich Store <FaGlassCheers></FaGlassCheers></Navbar.Brand>
                        </LinkContainer>

                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">

                                <LinkContainer to="/orders">
                                    <Nav.Link>My Orders</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/create">
                                    <Nav.Link>Create</Nav.Link>
                                </LinkContainer>

                                <LinkContainer to="/information">
                                    <Nav.Link>Contact us</Nav.Link>
                                </LinkContainer>


                            </Nav>
                            <Nav>                                
                                 <LinkContainer to="/cart">
                                 <Nav.Link>
                                     <FaShoppingCart size={40}/> <b>{orderItems.length}</b>                                     
                                 </Nav.Link>
                             </LinkContainer>
                                    
                                

                               

                            </Nav>












                        </Navbar.Collapse>
                    </Navbar>
                </Col>
            </Row>


            <Row>
                <Col><br></br> </Col>
                <Col> <br></br></Col>
                <Col> <br></br></Col>
            </Row>

            <Row>
                <Col><br></br> </Col>
                <Col> <br></br></Col>
                <Col> <br></br></Col>
            </Row>
            <Row>
                <Col><br></br> </Col>
                <Col> <br></br></Col>
                <Col> <br></br></Col>
            </Row>


            <Row>
                <Col><br></br> </Col>
                <Col> <br></br></Col>
                <Col> <br></br></Col>
            </Row>
        </Container>


    );
}


const mapStateToProps = ({ SandwichReducer }) => ({
    orderItems: SandwichReducer.orderItems,

})

const mapDispatchToProps = (dispatch) => ({



})




export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navigation);


